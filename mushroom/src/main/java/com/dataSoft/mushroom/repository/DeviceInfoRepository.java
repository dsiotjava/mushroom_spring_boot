package com.dataSoft.mushroom.repository;

import com.dataSoft.mushroom.model.DeviceInfo;
import com.dataSoft.mushroom.model.rowmapper.DeviceInfoRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Tausif on 9/24/2018.
 */

@Repository
public class DeviceInfoRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

//    public DeviceInfo findByUsername(String username) {
//        DeviceInfo user;
//        String query = "Select * from device_info where user_id like ? order by id desc limit 1";
//
//        try {
//            user = jdbcTemplate.queryForObject(query, new Object[] {username}, new DeviceInfoRowMapper());
//            return user;
//        } catch (DataAccessException dae) {
////            log.error("An exception occurred when executing the following query:");
////            log.error(query.replace("?", username));
////            log.error(dae.getLocalizedMessage());
//        }
//        return null;
//    }



    public List<DeviceInfo> findAll( int user_id) {

        List<DeviceInfo> deviceInfoList;
        String query = "Select * from device_info where user_id like ?";

        try {
            deviceInfoList = jdbcTemplate.query(query,new Object[] {user_id}, new DeviceInfoRowMapper());
            return deviceInfoList;
        } catch (DataAccessException dae) {
//            log.error("An exception occurred when executing the following query:");
//            log.error(query);
//            log.error(dae.getLocalizedMessage());
        }
        return null;
    }


}
