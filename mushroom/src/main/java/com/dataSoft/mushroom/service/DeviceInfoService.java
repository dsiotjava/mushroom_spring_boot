package com.dataSoft.mushroom.service;

import com.dataSoft.mushroom.model.DeviceInfo;
import com.dataSoft.mushroom.repository.DeviceInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Created by Tausif on 9/24/2018.
 */

@Service
public class DeviceInfoService {

    @Autowired
    private DeviceInfoRepository deviceInfoRepository;

//    public List<DeviceInfo> getAllDevice() {
//        List<DeviceInfo> deviceInfoList = deviceInfoRepository.findAll();
//        return deviceInfoList;
//    }


    public List<DeviceInfo> getAllDevice( int user_id) {
        List<DeviceInfo> deviceInfoList = deviceInfoRepository.findAll(user_id);
        return deviceInfoList;
    }

}
