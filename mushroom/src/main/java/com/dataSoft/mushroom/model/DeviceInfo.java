package com.dataSoft.mushroom.model;

import lombok.Data;

/**
 * Created by Tausif on 9/24/2018.
 */

@Data
public class DeviceInfo {

    int id;
    int user_id ;
    int device_id;

}
