package com.dataSoft.mushroom.model.rowmapper;

import com.dataSoft.mushroom.model.DeviceInfo;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by Tausif on 9/24/2018.
 */
public class DeviceInfoRowMapper implements RowMapper<DeviceInfo> {


    @Override
    public DeviceInfo mapRow(ResultSet resultSet, int i) throws SQLException {
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setId(resultSet.getInt("id"));
        deviceInfo.setUser_id(resultSet.getInt("user_id"));
        deviceInfo.setDevice_id(resultSet.getInt("device_id"));
        return deviceInfo;
    }
}
