package com.dataSoft.mushroom.controller;

import com.dataSoft.mushroom.model.DeviceInfo;
import com.dataSoft.mushroom.service.DeviceInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tausif on 9/24/2018.
 */

@RestController
@RequestMapping("/api")
public class DeviceInfoController {


    @Autowired
    private DeviceInfoService deviceInfoService;


    @GetMapping("/")
    public String home() {
        return "Welcome to Java Training";
    }


    //For checking whether Device configured or not for particular user.
    //ex: 182.163.112.207:96/api/device?id=7
    @GetMapping("/device")
    public List<DeviceInfo> getDeviceList(@RequestParam(value = "id", defaultValue = "0") int user_id){

        List<DeviceInfo>deviceInfoList = new ArrayList<>();

        deviceInfoList = deviceInfoService.getAllDevice(user_id);

        return deviceInfoList;

    }





}
